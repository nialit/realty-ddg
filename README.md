Uses python 3.7

Installed packages:

* tensorflow(Python 3.7 GPU support || Python 3.7 CPU only support) 
taken from here: https://www.tensorflow.org/install/pip

* pandas
* numpy
* seaborn
* matplotlib

Load postgres tables(there is an export func in postgres) as .csv with names of files same as names of tables

Put them in one folder, change path_to_csv in the code corresponding to the folder with csv's

Launch, it will make the model on `BoostedTreesRegressor`, try to change it's `max_depth` and `n_tree`' for better accuracy

You can specify model folder to reuse it later via `model_dir`

Use `predict` func on the model to make a prediction

There is tensorflow java API, what can be useful for integrating with java server \
I planned to launch model counting on Python, then using it in backend, if total loss func is better

TODO:
* make stats output about models accuracy
* make plotting of evalutions and real prices on evalution DS for more close cases investigations
