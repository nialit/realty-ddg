import tensorflow as tf
import pandas as pd
import numpy as np
import logging

from pandas import Series

if __name__ == '__main__':

    # physical_devices = tf.config.experimental.list_physical_devices('GPU')
    # assert len(physical_devices) > 0, "Not enough GPU hardware devices available"
    # tf.config.experimental.set_memory_growth(physical_devices[0], True)

    logging.getLogger("tensorflow").setLevel(logging.DEBUG)
    # tf.compat.v1.enable_eager_execution()

    # filenames = [path_to_csv + "prices.csv"]
    # record_defaults = [tf.int32, tf.int32, tf.int32]  # id, price, flat_id
    # pricesDs = tf.data.experimental.CsvDataset(filenames, record_defaults, header=True, select_cols=[1,2,4])

    # filenames = [path_to_csv + "flats.csv"]
    # record_defaults = [tf.int32] * 5 + [tf.string, tf.int32, tf.string]  # id, full_sq, kitchen_sq, life_sq, floor, is_apartment, building_id, closed
    # flatsDs = tf.data.experimental.CsvDataset(filenames, record_defaults, header=True, select_cols=[1,2,3,4,5,6,7,11])

    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1000)

    path_to_csv = "/home/natal/programming/realty/"

    prices: pd.DataFrame = pd.read_csv(path_to_csv + "prices.csv",
                                       usecols=["id", "price", "flat_id"],
                                       )
    flats: pd.DataFrame = pd.read_csv(path_to_csv + "flats.csv",
                                      usecols=["id", "full_sq",
                                               "kitchen_sq",
                                               "life_sq",
                                               "floor", "is_apartment",
                                               "building_id",
                                               # "closed"
                                               ],
                                      true_values="t", false_values="f", header=0)
    buildings: pd.DataFrame = pd.read_csv(path_to_csv + "buildings.csv",
                                          usecols=["id", "max_floor", "built_year", "flats_count",
                                                   # "address", #this comes from building_id
                                                   "renovation",
                                                   "has_elevator",
                                                   "district_id"  # nominative scale
                                                   ],
                                          true_values="t", false_values="f", header=0)

    time_to_metro: pd.DataFrame = pd.read_csv(path_to_csv + "time_metro_buildings.csv",
                                              usecols=["building_id", "time_to_metro", "transport_type"], header=0)
    time_to_metro = time_to_metro[time_to_metro['transport_type'] == "ON_FOOT"]

    ds: pd.DataFrame = pd.merge(prices, flats, left_on="flat_id", right_on="id")
    ds = pd.merge(ds, buildings, left_on="building_id", right_on="id")
    ds = pd.merge(ds, time_to_metro, left_on="building_id", right_on="building_id")

    ds = ds.drop(["transport_type", "id", "flat_id", "id_x", "id_y"], axis=1)
    ds.has_elevator = ds.has_elevator.astype(int)
    ds.renovation = ds.renovation.astype(int)
    ds.is_apartment = ds.is_apartment.astype(int)
    # ds = ds.replace(False, 0, inplace=True)
    # ds = ds.replace(True, 1, inplace=True)
    # print(ds.head())

    CATEGORICAL_COLUMNS = ['building_id', 'district_id', 'is_apartment', 'renovation', 'has_elevator']
    NUMERIC_COLUMNS = ["full_sq",
                       "kitchen_sq",
                       "floor",  # check out if is num scale
                       "max_floor", "flats_count", "time_to_metro",
                       "life_sq",
                       "built_year"
                       ]


    def one_hot_cat_column(feature_name, vocab):
        return tf.feature_column.indicator_column(
            tf.feature_column.categorical_column_with_vocabulary_list(feature_name,
                                                                      vocab))


    def get_normalization_parameters(traindf, features):
        """Get the normalization parameters (E.g., mean, std) for traindf for
        features. We will use these parameters for training, eval, and serving."""

        def _z_score_params(column):
            mean = traindf[column].mean()
            std = traindf[column].std()
            return {'mean': mean, 'std': std}

        normalization_parameters = {}
        for column in features:
            normalization_parameters[column] = _z_score_params(column)
        return normalization_parameters


    np.random.seed(13)
    msk = np.random.rand(len(ds)) < 0.8
    dftrain = ds[msk]
    dfeval = ds[~msk]
    y_train = dftrain.pop("price")
    y_eval = dfeval.pop("price")
    NUM_EXAMPLES = len(y_train)
    print(NUM_EXAMPLES)
    # print(dftrain.head(5))
    # print(dfeval.head(5))

    normalization_parameters = get_normalization_parameters(dftrain, NUMERIC_COLUMNS)

    feature_columns = []
    for feature_name in CATEGORICAL_COLUMNS:
        # Need to one-hot encode categorical features.
        vocabulary = ds[feature_name].unique()
        feature_columns.append(one_hot_cat_column(feature_name, vocabulary))


    def _make_zscaler(mean, std):
        def zscaler(col):
            return (col - mean) / std

        return zscaler


    for feature_name in NUMERIC_COLUMNS:
        t = ""
        scaler = _make_zscaler(
            normalization_parameters[feature_name]['mean'], normalization_parameters[feature_name]['std'])
        feature_columns.append(tf.feature_column.numeric_column(feature_name,
                                                                dtype=tf.float32,
                                                                normalizer_fn=scaler))


    # print(feature_columns)
    #
    # example = dftrain.head(2)
    # print(example)
    #
    # fc = tf.compat.v1.feature_column
    # print(fc.input_layer(dict(example), feature_columns).numpy())

    def make_input_fn(X, y, n_epochs=None, shuffle=True):
        def input_fn():
            dataset = tf.data.Dataset.from_tensor_slices((dict(X), y))
            if shuffle:
                dataset = dataset.shuffle(NUM_EXAMPLES)
            # For training, cycle thru dataset as many times as need (n_epochs=None).
            dataset = dataset.repeat(n_epochs)
            # In memory training doesn't use batching.
            dataset = dataset.batch(NUM_EXAMPLES)
            return dataset

        return input_fn


    def make_inmemory_train_input_fn(X, y):
        def input_fn():
            return dict(X), y

        return input_fn


    train_input_fn = make_inmemory_train_input_fn(dftrain, y_train)
    # Training and evaluation input functions.
    # train_input_fn = make_input_fn(dftrain, y_train)

    eval_input_fn2 = make_inmemory_train_input_fn(dfeval, y_eval)
    eval_input_fn = make_input_fn(dfeval, y_eval,
                                  shuffle=False, n_epochs=1
                                  )

    # for features_tensor, target_tensor in training_dataset:
    #     print(f'features:{features_tensor} target:{target_tensor}')

    # print(feature_columns)

    # print("GPU USED:" + str(tf.test.is_gpu_available(cuda_only=False, min_cuda_compute_capability=None)))

    # tf.config.set_soft_device_placement(True)
    #
    # tf.train.CheckpointManager.
    # tf.saved_model.
    est = tf.estimator.BoostedTreesRegressor(feature_columns,
                                             # model_dir="/tmp/tmpphcjt54y4", #here you can specify your model to take prev. results
                                             n_batches_per_layer=1,
                                             learning_rate=0.1,
                                             n_trees=150, # here
                                             max_depth=10, # and here you can try to increase values for better accuracy and slower model counting
                                             center_bias=True,
                                             train_in_memory=True)

    # The model will stop training once the specified number of trees is built, not
    # based on the number of steps.
    est.train(train_input_fn, max_steps=200)

    import matplotlib.pyplot as plt
    import seaborn as sns

    sns_colors = sns.color_palette('colorblind')
    pred_dicts = est.experimental_feature_importances(True)
    df_imp: Series = pd.Series(pred_dicts)

    # Visualize importances.
    N = 8
    ax = (df_imp.iloc[0:N][::-1]
          .plot(kind='barh',
                color=sns_colors[0],
                title='Gain feature importances',
                figsize=(10, 6)))
    ax.grid(False, axis='y')
    ax.plot()
    # ax.show()

    #here we get evaluations via our current model on eval set
    #this way you can also get result from any input params, e.g. from user input
    predicts = est.predict(eval_input_fn2)


    #Linear regression works bad
    # linear_est: tf.estimator.LinearRegressor = tf.estimator.LinearRegressor(feature_columns)
    #
    # # Train model.
    # linear_est.train(train_input_fn, steps=80000)
    #
    # # Evaluation.
    # result = linear_est.evaluate(eval_input_fn)

    # print(result)
